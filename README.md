# FutureGov in numbers 2015

## A small static site made with [Middleman](https://middlemanapp.com/)

Instructions for set up to run on [Heroku](https://www.heroku.com/) can be found [here.](https://jordanelver.co.uk/blog/2014/02/17/how-i-deployed-middleman-to-heroku/)

