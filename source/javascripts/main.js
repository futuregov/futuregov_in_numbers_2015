$( document ).ready(function() {

	var ctrl = new ScrollMagic.Controller({
    globalSceneOptions: {
      triggerHook: 'onLeave'
    }
  });

	var ctrl2 = new ScrollMagic.Controller({
    globalSceneOptions: {
      triggerHook: 'onEnter'
    }
  });

  $("section.panel").each(function() {
	  new ScrollMagic.Scene({
	      triggerElement: this
	  })
	  .setPin(this)
	  .addTo(ctrl);
  });

  $("section.panel").each(function() {
	  new ScrollMagic.Scene({triggerElement: ".intro h2"})
			.setClassToggle("h2", "active") // add class toggle
			.addTo(ctrl)
  });

  $(".count").fitText(1.1, { minFontSize: '20px', maxFontSize: '120px' });
  $(".heading").fitText(0.7);
  $(".subheading").fitText(1.7, { minFontSize: '20px', maxFontSize: '50px' });

});

